﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO_Task
{
    class algorithm
    {
        OrganicMode organic;
        SemidetatchedMode Semi;
        EmbededMode embeded;
        cocomoModes M;

        public algorithm()
        {
            organic = new OrganicMode();
            Semi = new SemidetatchedMode();
            embeded = new EmbededMode();
        }

        private static double Effort, Schedule, Productivity, averageStaffing;
        public static  String Type, Mode;
        private static double Size;

        public String getType()
        {
            return Type;
        }
        public void setType(String type)
        {
            Type = type;
        }
        public double getSize()
        {
            return Size;
        }
        public void setSize(double size)
        {
            Size = size/1000;
        }
        public String getMode()
        {
            return Mode;
        }
        public void setMode(String mode)
        {
            Mode = mode;
        }
        public double getEffort()
        {
            return Effort;
        }
        public void setEffort(double effort)
        {
            Effort = effort;
        }
        public double getSchedule()
        {
            return Schedule;
        }
        public void setSchedule(double schedule)
        {
            Schedule = schedule;
        }
        public double getProductivity()
        {
            return Productivity;
        }
        public void setProductivity(double productive)
        {
            Productivity = productive;
        }
        public double getavrgStaffing()
        {
            return averageStaffing;
        }
        public void setavrgStaffing(double avrgstaff)
        {
            averageStaffing = avrgstaff;
        }

        private void Model()
        {
            M= new cocomoModes();
            if (getMode().ToLower().Contains("organic"))
            {                
                M.cocomoMode(2.4, getSize(), 1.05, 2.5, getEffort(), 0.38);
            }
            else if (getMode().ToLower().Contains("semidetatched"))
            {
                M.cocomoMode(3, getSize(), 1.12, 2.5, getEffort(), 0.35);
            }
            else if (getMode().ToLower().Contains("embeded"))
            {
                M.cocomoMode(3.6, getSize(), 1.2, 2.5, getEffort(), 0.32);
            }
        }

        private void calculation()
        {
            double size = getSize();
            double effort = getEffort();
            double schedule =getSchedule();
            setProductivity(size / effort);
            setavrgStaffing(effort / schedule);
        }

        public void Sequence()
        {
            Model();
            calculation();
        }
    }
}
