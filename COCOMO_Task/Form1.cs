﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace COCOMO_Task
{
    public partial class Form1 : Form
    {
        algorithm algo = new algorithm();
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void Executebtn_Click(object sender, EventArgs e)
        {
            
            try
            {
                algo.setType(Type_txt.Text.ToString());
                algo.setSize(int.Parse(Size_txt.Text.ToString()));
                algo.setMode(Mode_comboBox.Text.ToString());
                
                algo.Sequence();
                //MessageBox.Show(algo.getEffort().ToString());
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
