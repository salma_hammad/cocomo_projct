﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace COCOMO_Task
{
    public class cocomoModes
    {
        algorithm algo;
        public void cocomoMode(double a, double KDSI, double b, double c, double effort, double d)
        {
            algo = new algorithm();
            algo.setEffort(a * (Math.Pow(KDSI, b)));
            algo.setSchedule(c * (Math.Pow(effort, d)));
        }
    }
}
