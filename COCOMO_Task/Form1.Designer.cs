﻿namespace COCOMO_Task
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Type_txt = new System.Windows.Forms.TextBox();
            this.Size_txt = new System.Windows.Forms.TextBox();
            this.Executebtn = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.Mode_comboBox = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label1.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(55, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(136, 44);
            this.label1.TabIndex = 0;
            this.label1.Text = "Type";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label2
            // 
            this.label2.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label2.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(55, 218);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(136, 44);
            this.label2.TabIndex = 1;
            this.label2.Text = "Mode";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label3.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(56, 301);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 44);
            this.label3.TabIndex = 2;
            this.label3.Text = "Size(KDSI)";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Type_txt
            // 
            this.Type_txt.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Type_txt.Location = new System.Drawing.Point(206, 137);
            this.Type_txt.Multiline = true;
            this.Type_txt.Name = "Type_txt";
            this.Type_txt.ReadOnly = true;
            this.Type_txt.Size = new System.Drawing.Size(357, 44);
            this.Type_txt.TabIndex = 3;
            this.Type_txt.Text = "Basic";
            // 
            // Size_txt
            // 
            this.Size_txt.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Size_txt.Location = new System.Drawing.Point(206, 303);
            this.Size_txt.Multiline = true;
            this.Size_txt.Name = "Size_txt";
            this.Size_txt.Size = new System.Drawing.Size(357, 44);
            this.Size_txt.TabIndex = 5;
            // 
            // Executebtn
            // 
            this.Executebtn.Font = new System.Drawing.Font("Monotype Corsiva", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Executebtn.Location = new System.Drawing.Point(87, 389);
            this.Executebtn.Name = "Executebtn";
            this.Executebtn.Size = new System.Drawing.Size(233, 56);
            this.Executebtn.TabIndex = 6;
            this.Executebtn.Text = "Execute";
            this.Executebtn.UseVisualStyleBackColor = true;
            this.Executebtn.Click += new System.EventHandler(this.Executebtn_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Monotype Corsiva", 21.75F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(460, 389);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(103, 56);
            this.button2.TabIndex = 7;
            this.button2.Text = "End";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label4
            // 
            this.label4.BackColor = System.Drawing.SystemColors.ControlLight;
            this.label4.Font = new System.Drawing.Font("Monotype Corsiva", 24F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label4.Location = new System.Drawing.Point(156, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(336, 44);
            this.label4.TabIndex = 8;
            this.label4.Text = "COCOMO Model";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // Mode_comboBox
            // 
            this.Mode_comboBox.AllowDrop = true;
            this.Mode_comboBox.Font = new System.Drawing.Font("Monotype Corsiva", 20.25F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Mode_comboBox.FormattingEnabled = true;
            this.Mode_comboBox.Items.AddRange(new object[] {
            "Organic Mode",
            "Semidetatched Mode",
            "Embeded Mode"});
            this.Mode_comboBox.Location = new System.Drawing.Point(206, 218);
            this.Mode_comboBox.Name = "Mode_comboBox";
            this.Mode_comboBox.Size = new System.Drawing.Size(357, 41);
            this.Mode_comboBox.TabIndex = 9;
            this.Mode_comboBox.Text = "Mode";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(616, 540);
            this.Controls.Add(this.Mode_comboBox);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.Executebtn);
            this.Controls.Add(this.Size_txt);
            this.Controls.Add(this.Type_txt);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox Type_txt;
        private System.Windows.Forms.TextBox Size_txt;
        private System.Windows.Forms.Button Executebtn;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox Mode_comboBox;
    }
}

